package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        String entradaTeclado = "";
        Scanner entradaScanner = new Scanner(System.in);
        System.out.println("Palabra o frase a comprobar: ");
        entradaTeclado = entradaScanner.nextLine();
        Palindromo palin = new Palindromo();

        if(palin.comprobar(entradaTeclado)) {
            System.out.println("Es palindromo");
        }else{
            System.out.println("No es palindromo");
        }

    }
}
